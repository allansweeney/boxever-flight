
Application

1. Download the code and open a command window the applications folder
2. Install the npm modules: npm install
3  Install bower: npm install -g bower
4. Install the bower components: bower install
5. Start the server: node server.js
6. Visit the application in your browser at http://localhost:8080

Database setup

1. Create the data folder in the BoxeverFlightApplication root directory.
2. Connect to the database: 

      e.g. mongod --dbpath C:\node\BoxeverFlightApplication\data

3. Open a separate shell and create the flightdata MongoDB database:

       >mongo
       >use flightdata

4. Populate the application with sample data using the mongodb script contained in the project:

       e.g. load("C:/node/BoxeverFlight/boxeverdb.js")