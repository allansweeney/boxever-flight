var Flight = require('./models/flight');
var Booking = require('./models/booking');

module.exports = function(app) {

	
	//api---------------------------------------------------------------------
	
	//get all bookings
	app.get('/api/bookings', function(req, res) {
		// use mongoose to get all bookings in the database
		Booking.find(function(err, bookings) {
			// if there is an error retrieving, send the error. nothing after res.send(err) will execute
			if (err)
				res.send(err)
			res.json(bookings); // return all bookings in JSON format
		});
	});

	// get all flights
	app.get('/api/searchFlights', function(req, res) {
		// use mongoose to get all flights in the database
		Flight.find(function(err, flights) {

			// if there is an error retrieving, send the error. nothing after res.send(err) will execute
			if (err)
				res.send(err)

			res.json(flights); // return all flights in JSON format
		});
	});


	// create a booking
	app.post('/api/createBooking', function(req, res) {
		console.log(req.body.departureDate);
		// create a booking, information comes from AJAX request from Angular
		Booking.create({
			origin : req.body.origin,
			destination : req.body.destination,
			departureDate : req.body.departureDate,
			returnDate : req.body.returnDate,
			flightNumber : req.body.flightNumber,
			flightClass : req.body.flightClass,
			price : req.body.price,
			time : req.body.time,
			passengerName: req.body.passengerName,
			email: req.body.email
		}, function(err, booking) {
			if (err)
				res.send(err);
				console.log(err);
			// get and return all the bookings after you create another
			Booking.find(function(err, bookings) {
				// if there is an error retrieving, send the error. nothing after res.send(err) will execute
				if (err)
				res.send(err)
				res.json(bookings); // return all bookings in JSON format
			});
		});
	});
	
	app.get('*', function(req, res) {
		res.sendfile('./public/index.html');
	});

};