var mongoose = require('mongoose');

module.exports = mongoose.model('Flight', {
	origin : String,
	destination : String,
	departureDate : String,
	returnDate : String,
	flightNumber : String,
	flightClass : String,
	price : String,
	select : String
});