var mongoose = require('mongoose');

module.exports = mongoose.model('Booking', {
	origin : String,
	destination : String,
	returnDate : String,
	departureDate : String,
	flightNumber : String,
	flightClass : String,
	price : String,
	time : String,
	passengerName: String,
	email: String
});