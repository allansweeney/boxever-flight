//Wires the views to the controllers
angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/booking', {
			templateUrl: 'views/booking.html',
			controller: 'BookingController'
		})

		.when('/enterBookingDetails', {
			templateUrl: 'views/enterBookingDetails.html',
			controller: 'EnterBookingController'
		})

		.when('/bookingConfirmation', {
			templateUrl: 'views/bookingConfirmation.html',
			controller: 'BookingConfirmationController'
		})

		.when('/bookingList', {
			templateUrl: 'views/bookingList.html',
			controller: 'BookingListController'	
		});

	$locationProvider.html5Mode(true);

}]);