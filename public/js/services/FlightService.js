//service to handle flight related database calls
angular.module('FlightService', []).factory('Flight', ['$http', function($http) {
	
	return {
		// call to get all flights
		get : function() {
			return $http.get('/api/searchFlights');
		}
	}
}]);