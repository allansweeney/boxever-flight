//supports the booking process flow related to data retrieval and state
angular.module('BookingService', []).factory('Booking', ['$http', function($http) {
	
	var selectedFlight = "";
	var bookingsinHour = 0;
	var sumPrice = 0;
	var origin = "";
	var destination = "";
	var userDetails = {};
	var userConfirmation = {};

	return {
		//gets all bookings
		get : function() {
			return $http.get('/api/bookings');
		},
		//creates a booking
		create : function(bookingData) {
			return $http.post('/api/createBooking', bookingData);
		},
		//stores passenger form data
		updatePassengerDetails : function(updatedDetails) {
	      	userDetails.passengerName = updatedDetails.passengerName;
	      	userDetails.email = updatedDetails.email;
		},
		//retrieves passenger name
		getPassengerName : function(){
        	return userDetails.passengerName;
		},
		//retrieves passenger email
		getEmail : function(){
        	return userDetails.email;
		},
		//stores the current selected flight
		updateSelectedFlight : function(flightId) {
	      	selectedFlight = flightId;
		},
		//retrieves the currently selected flight
		getSelectedFlight : function(){
        	return selectedFlight;
		}
		,
		//maintains hourly stats on bookings and sum total
		updateHourlyStats : function(totalBookings){
			var tempBookings = 0;
			var totalPrice = 0;

			angular.forEach(totalBookings, function(booking) {

				if ( Math.floor((new Date() - booking.time)/60000) < 60 ) {
					tempBookings++;
					totalPrice = +totalPrice + +booking.price;
				}
		    });

		    bookingsinHour = tempBookings;
		    sumPrice = totalPrice;
		},
		//retrieves hourly bookings
		getBookingsInHour : function(){
        	return bookingsinHour;
		},
		//retrieves hourly sum price
		getSumPrice : function(){
        	return sumPrice;
		},
		//updates the flight
		updateFlightDetails : function(flightDetails){
			origin = flightDetails.origin;	
        	destination = flightDetails.destination;
		},
		//retrieves current selected flight info
		getOrigin : function(){
        	return origin;
		},
		//retrieves current selected flight info
		getDestination : function(){
        	return destination;
		},
		//resets the form state
		resetFormData : function(){
        	selectedFlight = "";
			origin = "";
			destination = "";
			userDetails.passengerName = "";
			userDetails.email = "";
		},

		//stores flight confirmation information
		addConfirmation : function(confirmationData){
			console.log(confirmationData.passengerName);
			userConfirmation.passengerName = confirmationData.passengerName;
			userConfirmation.origin = confirmationData.origin;
			userConfirmation.destination = confirmationData.destination;
			userConfirmation.departureDate = confirmationData.departureDate;
		},

		//retrieves flight confirmation information
		getConfirmation : function(){
        	return userConfirmation;
		},
		//clears confirmation information when the user leaves the page
		flushConfirmation : function(){
        	userConfirmation = {};
		}
	}

}]);
