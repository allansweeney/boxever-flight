//handles the rendering of the booking list page
angular.module('BookingListCtrl', []).controller('BookingListController', function($scope, $http, Flight, Booking) {

	//retrieve scope variables from the service
	$scope.bookingsThisHour = Booking.getBookingsInHour();
	$scope.sumPriceThisHour = Booking.getSumPrice();
	
	//When the view loads we want to fetch current bookings
	Booking.get()
		.success(function(data) {
			$scope.bookings = data;
	});

});