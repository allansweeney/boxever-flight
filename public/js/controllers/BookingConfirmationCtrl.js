//handles the rendering of the confirmation page
angular.module('BookingConfirmationCtrl', []).controller('BookingConfirmationController', function($location, $scope, Booking) {

	$scope.tagline = 'Have a nice trip!';

	//get the details from the service
	$scope.confirmationDetails = Booking.getConfirmation();

	//if the service field is blank, route to homepage
	if($scope.confirmationDetails.origin == null || $scope.confirmationDetails.origin.length <= 0){
		$location.path( "/" );
	}
});