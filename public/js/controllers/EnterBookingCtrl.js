//Enters a booking into the database
angular.module('EnterBookingCtrl', []).controller('EnterBookingController', function($location, $scope, $http, Flight, Booking) {

	//initialize state
	Booking.updateHourlyStats($scope.bookings);
	Booking.flushConfirmation();

	$scope.DisplayConfirm = false;
	$scope.formData = {};
	$scope.selectedFlight = Booking.getSelectedFlight();
	$scope.passengerName = Booking.getPassengerName(); 
    $scope.email = Booking.getEmail();

	$scope.userDetails = {
        passengerName: $scope.passengerName,
        email: $scope.email
    };

	//creates a booking in the database
	$scope.createBooking = function() {

		var currentDateTime = new Date();

		//add flight information to the form
		angular.forEach($scope.flights, function(flight) {
	        if (flight._id == $scope.selectedFlight){
	      		$scope.formData.passengerName = $scope.userDetails.passengerName;
				$scope.formData.email = $scope.userDetails.email;
	      		$scope.formData.origin = flight.origin;
				$scope.formData.destination = flight.destination;
				$scope.formData.departureDate = flight.departureDate;
				$scope.formData.returnDate = flight.returnDate;
				$scope.formData.flightNumber = flight.flightNumber;
				$scope.formData.flightClass = flight.flightClass;
				$scope.formData.price = flight.price;
				$scope.formData.time = currentDateTime.getTime();
	        }
	    });

		// call the create function from our service (returns a promise object)
		Booking.create($scope.formData)
			// if successful creation, call our get function to get all the new bookings
			.success(function(data) {
				//reset the forms
				$scope.selectedFlight = {}
				Booking.resetFormData();
				// update the list of bookings
				$scope.bookings = data; 
				//update state
				Booking.addConfirmation($scope.formData);
				Booking.updateHourlyStats($scope.bookings);
				$location.path( "/bookingConfirmation" );
			});
	};

	//load the passenger details into the service
	$scope.updatePassengerDetails = function() {
		Booking.updatePassengerDetails($scope.userDetails)
	}
});