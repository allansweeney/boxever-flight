//handles the flight selection and booking process
angular.module('BookingCtrl', []).controller('BookingController', function($scope, $http, Flight, Booking) {

	//initialize state
	Booking.flushConfirmation();
	$scope.selectedFlight = Booking.getSelectedFlight();
	$scope.origin = Booking.getOrigin(); 
	$scope.destination = Booking.getDestination();

	$scope.search = {
        origin: $scope.origin,
        destination: $scope.destination,
    };

	//retrieve list of flights
	Flight.get()
		.success(function(data) {
			$scope.flights = data;
			if($scope.selectedFlight.length > 1){
				//determine if the flight was previously selected
				angular.forEach($scope.flights, function(flight) {
		        	if (flight._id == $scope.selectedFlight){
						flight.selected = "true";
						$scope.DisplayEnterDetails = true;
					}
		    	});
			}
		});

	//When the view loads we want to fetch current bookings
	Booking.get()
		.success(function(data) {
			$scope.bookings = data;
			//initialize hourly stats
			Booking.updateHourlyStats($scope.bookings);
	});
	//loads the selected flight into the service
	$scope.selectFlight = function(flightId) {
		Booking.updateSelectedFlight(flightId);
		$scope.DisplayEnterDetails = true;
	}
	//loads the flight details into the service
	$scope.updateFLightSelection = function() {
		Booking.updateFlightDetails($scope.search)
	}
});

